/**	@class	LitElementX
	@classdesc
		Base class for view components.
	@extends	LitElement
	@mixes	BreakpointMixin
	@mixes	Polymer-Redux-Bindings
	@requires	lit-element
	@since	1.6.0
	@author	Castle Rock Associates <contact@crc-corp.com>
	@license	GPL-2.0-or-later
	@summary
		Includes callbacks for responding to state changes, particularly breakpoints and routing.
**/
import { LitElement, html, css } from 'lit-element'
class LitElementX extends LitElement {
	
	/**	@property styles
		@description
			this is a description of some stuff
		@memberof LitElementX#
		@returns Array
	**/
	static get styles(){	return [
		css`:host	{	display:block;	}`
	]}
	
	/**	@function render
		@description
			The component's HTML should be defined here, using data binding to populate it from observed properties defined in {@link LitElementX#getproperties} below. 
		@memberof LitElementX#
		@returns TemplateResult
	**/
	render(){	return html`
	
	`}
	
	/**	@function get properties
		@summary
			static accessor for defining properties that are 'watched' to trigger an update
		@description
			this is a description of some stuff  
		@memberof LitElementX#
		@returns TemplateResult
	**/
	static get properties(){	return {
		foo:{type:String,reflect:true}
	}}
	
	/**	@function constructor
		@summary
			Initialize property values.
		@memberof LitElementX#
	**/
	constructor(){	super()
		this.foo = 'bar'
	}
	
	firstUpdated(props){	super.firstUpdated(props)
	
	}
	
	updated(props){	super.updated(props)
	
	}
	
	stateChange(state){
		
	}
}