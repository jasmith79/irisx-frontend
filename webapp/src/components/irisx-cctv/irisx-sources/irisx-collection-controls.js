import { LitElement, html } from '@polymer/lit-element'
import { store } from '../../../redux/redux-store.js';
import { setMobileState, CAMERAS } from '../../../redux/redux-MobileState.js';
import { DetailsModMixin } from '../../../utils/details-mod-mixin.js'
import CustomComposedEvent from '../../../utils/CustomComposedEvent.js';
import { ConfigCCTV } from '/config/Config.js'
import { GeoSearch } from '../../../rest/Geosearch'

import CSSReset from '../../css/lib/reset.css.js'
import BButtonBase from '../../css/lib/bbuttonbase.css.js'
import SharedStyles from '../../css/shared.css.js'
import CollectionControlsStyles from './collectioncontrols.css.js'

window.customElements.get('irisx-collection-controls') || window.customElements.define('irisx-collection-controls',
class extends DetailsModMixin(LitElement) {
    render() {  return html`

        ${CSSReset}
        ${BButtonBase}
        ${SharedStyles}
        ${CollectionControlsStyles}

        <header id="sources-topbar">
            
            <div id="mobile-modal">
                <h1>Search</h1>
                <button type="button" id="closebtn" class="bbuttonbase" title="close" @click="${this._closeButtonClick}"><img src="img/icon-close-dark.svg" alt="close" width="12" height="12" /></button>
            </div>

        <!--    VIEW / TABLE TOGGLE BUTTON - DESKTOP ONLY   -->

        <lit-route path="/cctv/:group/map" >
            <a href="/cctv/${this.group}/table" title="Table View" id="sources-view-toggle-link"><img src="img/icon-table-fill.svg" alt="Table View" width="28" height="24" /><span class="hide-on-mid">Table View</span></a>
        </lit-route>
        <lit-route path="/cctv/:group/table">
            <a href="/cctv/${this.group}/map" title="Map View" id="sources-view-toggle-link"><img src="img/icon-map-fill.svg" alt="Map View" width="28" height="24" /><span class="hide-on-mid">Map View</span></a>
        </lit-route>

            <form id="sources-form" role="search" @submit="${this._searchFormSubmit}" @invalid="${{handleEvent:e=>this._invalidateSearch(e),capture:true}}">
                <fieldset id="combobox-search-fields">
                    <div class="legend-wrapper"><legend>
                        <img src="img/icon-search-fill.svg" alt="search" width="28" height="28" />
                    </legend></div>
                    <div id="combobox-container">
                        <div id="combobox-input-container">

                        <!--    GEOSEARCH FOR MAP   -->

                        <lit-route path="/cctv/:group/map">
                            <input type="search" name="search" title="Search for a location" @input="${this._geosearchInput}" id="geosearch" autocomplete="off" required />
                            <label for="search">Search for a location</label>
                            <ol id="geosearch-results">${this.geosearchResults.map(result=>html`
                                <li><button type="button" class="bbuttonbase" value="${JSON.stringify(result)}" @click="${this._onSearchResultClick}">${result.label}</button></li>
                            `)}</ol>
                        </lit-route>

                        <!--    CAMERA SEARCH FOR TABLE   -->

                        <lit-route path="/cctv/:group/table">
                            <input type="search" name="search" title="Search for a camera" @input="${this._cameraSearchInput}" id="camerasearch" autocomplete="off" required />
                            <label for="search">Search for a camera</label>
                            <button type="submit" class="bbuttonbase" id="cameraSearchSubmit"><span>Search</span></button>
                        </lit-route>
                        </div>

                        <!--    PREDEFINED GEO LOCATION SELECTOR FOR MAP   -->

                    <lit-route path="/cctv/:group/map">
                        <details id="combobox-location-selector" class="details-mod"><summary><img src="img/icon-caret-fill.svg" alt="Search Regions" width="16" height="8" /><span>Search Regions</span></summary>
                            <div class="details-content">
                                <strong>Search Regions</strong>
                                <ul>${ConfigCCTV.Map.Locations.map(Location=>html`<li><button type="button" class="bbuttonbase" value="${JSON.stringify(Location)}" @click="${this._locationSelect}"><span>${Location.name}</span></button></li>`)
                                }</ul>
                            </div>
                        </details>
                    </lit-route>
                    </div>
                </fieldset>

                <!--    FILTER BY CAMERA TYPE   -->

                <fieldset id="filter-search-fields">
                    <details class="details-mod"><summary><img src="img/icon-filter-fill.svg" alt="Filter" width="22" height="22" /><span class="hide-on-mid">Filter</span></summary>
                        <div class="details-content">
                            <legend>Filter Results</legend>
                            <ul>
                                <li><label><input type="checkbox" name="filtertypes" @change="${this._filterChange}" value="CCTV" ?checked="${(this.cameraTypes.includes('CCTV')) ? true : false}" /><span>CCTV</span></label></li>
                                <li><label><input type="checkbox" name="filtertypes" @change="${this._filterChange}" value="HOOSIERHELPER" ?checked="${(this.cameraTypes.includes('HOOSIERHELPER')) ? true : false}" /><span>Hoosier Helper</span></label></li>
                            </ul>
                        </div>
                    </details>
                </fieldset>
            </form>
        </header>
    `}

    static get properties() {
        return {
            view:{type:String,reflect:true},
            group:{type:String,reflect:true},
            mobilestate:{type:String,reflect:true},
            cameraTypes:{type:Array},
            searchInputBuffer:{type:Number},
            searching:{type:Boolean,reflect:true},
            geosearchInput:{type:Object},
            geosearchResults:{type:Array}
        }
    }

    constructor(...args){   super(...args)
        this.searching = false
        this.geosearchResults = []
    }

    firstUpdated(){
        this.geosearchInput = this.shadowRoot.getElementById('geosearch')
    }

    getFilterTypes(){
        return Array.from(this.shadowRoot.querySelectorAll('input[type=checkbox][name=filtertypes]:checked')).map(input=>input.value)
    }

    _formatLabel(propObj){
        let label = propObj.name
        if(propObj.city || propObj.state)   label += ' - '
        if(propObj.city)    label += propObj.city
        if(propObj.city && propObj.state)    label += ', '
        if(propObj.state)    label += propObj.state
        return label
    }

    _invalidateSearch(e){
        switch(this.view){
            case 'map':
                this._geosearchInput()
            break
            case 'table':
                this._searchFormSubmit()
            break
            default:break
        }
        return e.preventDefault()
    }   //  we're only using html validation for the styling hooks here

    _geosearchInput(){  //  GEO SEARCH FOR MAP
        this.geosearchResults = []
        clearTimeout(this.searchInputBuffer)
        this.searchInputBuffer = setTimeout(()=>{
            if(!this.searching){
                this.searching = true
                GeoSearch.search(this.geosearchInput.value).then(results=>{
                    this.geosearchResults = results.map(result=>({
                        label:this._formatLabel(result.properties),
                        lat:result.geometry.coordinates[1],
                        lon:result.geometry.coordinates[0]
                    }))
                    this.searching = false
                })               
            }
        },300)
    }

    _cameraSearchInput(){   //  CAMERA SEARCH FOR TABLE
        clearTimeout(this.searchInputBuffer)
        this.searchInputBuffer = setTimeout(()=>{
            this.dispatchEvent(new CustomComposedEvent('cameraSearch',{query:this.shadowRoot.getElementById('camerasearch').value}))
        },300)
    }

    _searchFormSubmit(e){   //  proxies above for mobile functionality
        this._cameraSearchInput()
        store.dispatch(setMobileState(CAMERAS))
        return (e) ? e.preventDefault() : false
    }

    _onSearchResultClick(e){
        let result = JSON.parse(e.target.value)
        this.dispatchEvent(new CustomComposedEvent('mapToLocation',{location:{
                //  this is such an opaque process
            NorthLat:result.lat,
            EastLon:result.lon,
            SouthLat:result.lat,
            WestLon:result.lon
        }}))
        this.shadowRoot.getElementById('geosearch').value = ''
        this.shadowRoot.getElementById('sources-form').search.value = result.label
        this.geosearchResults = []
        store.dispatch(setMobileState(CAMERAS))
    }

    _locationSelect(e){
        let location = JSON.parse(e.currentTarget.value)
        this.shadowRoot.getElementById('sources-form').search.value = location.name
        this.shadowRoot.getElementById('combobox-location-selector').open = false
        this.geosearchResults = []
        this.dispatchEvent(new CustomComposedEvent('mapToLocation',{location:location}))
        store.dispatch(setMobileState(CAMERAS))
    }

    _filterChange(){
        this.dispatchEvent(new CustomComposedEvent('cameraTypesChange',{cameraTypes:this.cameraTypes = this.getFilterTypes()}))
    }

    _closeButtonClick(){    store.dispatch(setMobileState(CAMERAS)) }
})