    //  ext lib
import { LitElement, html } from '@polymer/lit-element'
import { store } from '../../../redux/redux-store'
import { navigate } from 'lit-redux-router'
    //  int lib
import { StringIfPropIsVal }  from '../../../utils/utils'
import CustomComposedEvent from '../../../utils/CustomComposedEvent'
    //  css
import CSSReset from '../../css/lib/reset.css.js'
import BButtonBase from '../../css/lib/bbuttonbase.css.js'
import SharedStyles from '../../css/shared.css.js'
import MobileMenuStyles from './mobilemenu.css.js'
import { CAMERAS, MONITORS, FILTERS, setMobileState } from '../../../redux/redux-MobileState';
    //  web component
window.customElements.get('irisx-sources-mobile-menu') || window.customElements.define('irisx-sources-mobile-menu', 
class extends LitElement {
    render(){   return html`

        ${CSSReset}
        ${BButtonBase}
        ${SharedStyles}
        ${MobileMenuStyles}

        <nav><ul>
            <li>
                <button type="button" class="bbuttonbase ${StringIfPropIsVal('active',`${this.mobilestate}_${this.view}`,CAMERAS+'_table')}" id="tableButton" title="Table" value="table" @click="${this._mobileViewClick}">
                </button>
            </li>
            <li class="${StringIfPropIsVal('disabled',this.mobilestate,MONITORS)}">
                <button type="button" class="bbuttonbase" id="searchButton" title="Search" value="search" @click="${this._mobileSearchClick}">
                    <img src="img/icon-search-fill.svg" alt="Search" width="28" height="28" />
                </button>
            </li>
            <li>
                <button type="button" class="bbuttonbase ${StringIfPropIsVal('active',`${this.mobilestate}_${this.view}`,CAMERAS+'_map')}" id="mapButton" title="Map" value="map" @click="${this._mobileViewClick}">
                </button>
            </li>
        </ul></nav>

    `}

    static get properties(){    return {
        group:{type:String,reflect:true},
        view:{type:String,reflect:true},
        mobilestate:{type:String,reflect:true}
    }}

    _mobileSearchClick(){    if(this.mobilestate === CAMERAS)  this._setMobileState(FILTERS)  }

    _mobileViewClick(e){
        if(this.mobilestate === CAMERAS && this.view === e.currentTarget.value){
            this._setMobileState(MONITORS)
        }else{
            this._setMobileState(CAMERAS)
            store.dispatch(navigate(`/cctv/${this.group}/${e.currentTarget.value}`))
        }
    }

    _setMobileState(_mobilestate) {
        store.dispatch(setMobileState(this.mobilestate = _mobilestate))
    }
})