
/*
 |--------------------------------------------------------------------------
 | Browser-sync config file
 |--------------------------------------------------------------------------
 |
 | For up-to-date information about the options:
 |   http://www.browsersync.io/docs/options/
 |
 |
 */
module.exports = {
    "files":[
        "./src/**/*.js",
        "./index.*",
        "./package.json",
        "./polymer.json",
        "./manifest.json",
        "./sw-precache-config.js",
        "./config/**/*",
		"./img/**/*"
    ],
    "ignore": [
        "node_modules",
        ".git",
        "build",
        "server",
        "*.map"
    ],
    "open":false,
    reloadDebounce: 100,
    "server": false,
    "notify": false,
    "injectNotification": false,
    "tagNames": {
        "less": "link",
        "scss": "link",
        "css": "link",
        "jpg": "img",
        "jpeg": "img",
        "png": "img",
        "svg": "img",
        "gif": "img",
        "js": "script"
    }
}