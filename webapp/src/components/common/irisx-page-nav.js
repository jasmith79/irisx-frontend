import { LitElement, html } from '@polymer/lit-element'
import { DetailsModMixin } from '../../utils/details-mod-mixin.js'
import { BreakpointMixin } from '../../utils/breakpoint-mixin.js'
import { StringIfPropIsVal } from '../../utils/utils'

import { ConfigIRISx } from '/config/Config.js'

import CSSReset from '../css/lib/reset.css.js'
import BButtonBase from '../css/lib/bbuttonbase.css.js'
import SharedStyles from '../css/shared.css.js'
import PageNavStyles from './pagenav.css.js'

function menuHideBlocker(){ return window.innerWidth < 820  }   //  magic number cause we can't access breakpoint mixin'd props unforunately
let detailsModMixinConfig = {   handlers:[
    {   selector:'#menu-container',
        documentClickHandler:menuHideBlocker,
        summaryBlurHandler:menuHideBlocker,
        summaryClickHandler:menuHideBlocker
    }
]}

window.customElements.get('irisx-page-nav') || window.customElements.define('irisx-page-nav',
class extends DetailsModMixin(BreakpointMixin(LitElement),detailsModMixinConfig){

    _renderMenuItem(item){  return html`
        <li class="${StringIfPropIsVal('active',this.page,item.pageID)} ${StringIfPropIsVal('active',item.pageID,'logout')}"><a href="${item.href}" title="${item.label}">
            <div><img src="${item.icon}" alt="${item.label}" width="24" height="24" /></div>
            <span>${item.label}</span>
        </a></li>
    `}

    render(){  return html`
    
        ${CSSReset}
        ${BButtonBase}
        ${SharedStyles}
        ${PageNavStyles}

        <header id="topbar">
            <h1 id="logo-link"><a href="${ConfigIRISx.SuperMenu.Logo.href}" title="Home"><img src="${ConfigIRISx.SuperMenu.Logo.icon}" alt="${ConfigIRISx.SuperMenu.Logo.alt}" width="64" height="64" /></a></h1>
            <h2 id="page-title">${ConfigIRISx.Pages[this.page].pageTitle}</h2>
            <details open id="menu-container" class="details-mod"><summary><span>Menu</span></summary>
                <nav class="details-content">
                    <ul class="left-links">${ConfigIRISx.SuperMenu.Left.map(this._renderMenuItem)}</ul>
                    <ul class="right-links">
                        <li><a href="/logout" title="Sign Out" id="signout-link">
                            <div><img src="img/icon-profile-fill-solid.svg" alt="Sign Out" width="36" height="36" /></div>
                            <span>Sign Out</span>
                        </a></li>
                    </ul>
                </nav>
            </details>
        </header>
    `}

    static get properties(){   return {
        page:{type:String,reflect:true},
		menuContainer:{type:Object}
    }}

    constructor(...args){   super(...args)
        this._renderMenuItem = this._renderMenuItem.bind(this)
    }

    firstUpdated(...args){  super.firstUpdated(...args)
		this.menuContainer = this.shadowRoot.getElementById('menu-container')
        this._breakpointHandler()   //  we mixed this in earlier, remember?
    }

    _setMenuState(state){   if(this.menuContainer)  this.menuContainer.open = state   }

    onMobileBreakpoint(...args){  super.onMobileBreakpoint(...args)
        this._setMenuState(false)
    }

    onDesktopBreakpoint(...args){  super.onDesktopBreakpoint(...args)
        this._setMenuState(true)
    }
})