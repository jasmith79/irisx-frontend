import { setPassiveTouchGestures } from '@polymer/polymer/lib/utils/settings.js'
import { LitElement, html } from '@polymer/lit-element'
import { store } from '../redux/redux-store'

import { connect } from 'pwa-helpers/connect-mixin.js'
import { connectRouter, navigate } from 'lit-redux-router'
import { clearItems } from '../redux/redux-LoaderAnim'

import { ConfigIRISx } from '/config/Config.js'
import { Authorization } from '../rest/Authorization.js'

import CSSReset from './css/lib/reset.css.js'
import LAStyles from './css/lib/LoadAwesomeAnimations.css.js'
import SharedStyles from './css/shared.css.js'
import AppStyles from './app.css.js'

window.customElements.get('irisx-app') || window.customElements.define('irisx-app',
class extends connect(store)(LitElement) {
  render() {  return html`

    ${CSSReset}
    ${LAStyles}
    ${SharedStyles}
    ${AppStyles}

    <div class="loading-modal" id="loading-container">
      <div class="animation-container">
        <div style="color: #f6d860" class="la-root la-ball-pulse la-3x" @click="${e=>e.preventDefault()}">
          <div></div><div></div><div></div>
        </div>
        <span class="loading-message">Loading, please wait... (${this.loaderLength})</span>
      </div>
    </div>

    <lit-route path="/login"><irisx-login></irisx-login></lit-route>
    <lit-route path="/cctv/:group/:view"><irisx-cctv page="${this.page}"></irisx-cctv></lit-route>
    <lit-route path="/dms"><irisx-dms></irisx-dms></lit-route>
    <lit-route path="/cad"><irisx-cad></irisx-cad></lit-route>
  `}

  static get properties(){  return {
    page:{type:String},
    currentRoute:{type:String},
    loadingContainer:{type:Object},
    loaderLength:{type:Number}
  }}

  constructor(){  super()
    setPassiveTouchGestures(true)
    connectRouter(store)  //  downstream elements use the connect-mixin to inherit the stateChanged method
  }

  firstUpdated(props){ super.firstUpdated(props)
    this.loadingContainer = this.shadowRoot.getElementById('loading-container')
  }

  stateChanged(state){  super.stateChanged(state)
      //  route change dedupe
    if(
      JSON.stringify(state.router.routes) !== '{}' &&
      state.router.activeRoute !== this.currentRoute
    ){
      this._topLevelRouting(state.router.activeRoute)
    }

    if(state.LoaderAnim)  this._processLoaderQueue(state.LoaderAnim)
  }

  _processLoaderQueue(_loader){
    this.loaderLength = _loader.total
    if(this.loadingContainer){
      if(this.loaderLength > 0){        
        this.loadingContainer.classList.add('show')
      }else{  //  nothing is waiting to load
        this.loadingContainer.classList.remove('show')
      }
    }
  }

  _topLevelRouting(_currentRoute){
    this.currentRoute = _currentRoute
    let routeSegments = this.currentRoute.split('/').filter(value=>value!=='')  //  get rid of extra blank bits
    let _page = routeSegments[0] || ''  //  'page' is all the top level app cares about

      //  top level page routing
      
    if(this.page !== _page){  //  page change dedupe

      if(_page !== 'login' && !Authorization.JWT){  //  not logged in and not logging in
        return store.dispatch(navigate('/login'))  //  too bad for you buckaroo you're logging in like it or not
      }else if(_page === 'logout'){
        Authorization.logout()
        return store.dispatch(navigate('/login'))
      }else if(_page === ''){
        return store.dispatch(navigate('/login')) //  default page
      }

      let Page = ConfigIRISx.Pages[this.page = _page]

      if(Page !== undefined){ //  valid page
        store.dispatch(clearItems())
        document.title = `${ConfigIRISx.AppTitle} - ${Page.pageTitle}`
        if(Page.component)  import(Page.component)
      }else{
        console.error(`invalid page: '${_page}'`)
        return store.dispatch(navigate('/login'))
      }
    }
  }
})