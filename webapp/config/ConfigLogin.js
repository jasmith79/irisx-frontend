/* ⚠    KEEP THIS FILE! If you need to make changes, copy and rename this default version for future reference!   ⚠ */

window.__IRISXConfig.Login = {
	Logo:{
		icon:'/config/logo.png',
		alt:'Indiana Department of Transportation',
		href:'/'    //  clicking the link goes here
	}
}