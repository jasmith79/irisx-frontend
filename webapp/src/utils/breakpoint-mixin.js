const breakpointWidth = 820
export const BreakpointMixin = (BaseClass, autoInit=true)=>class extends BaseClass {

        //  mix

    constructor(){   super()
        this._breakpointHandler = this._breakpointHandler.bind(this)
        this.lastBreakpointName = null
    }

        //  it

    connectedCallback(){    super.connectedCallback()
        window.addEventListener('resize',this._breakpointHandler,{passive:true})
    }
    disconnectedCallback(){ super.disconnectedCallback()
        window.removeEventListener('resize',this._breakpointHandler,{passive:true})
    }

    firstUpdated(props){    super.firstUpdated(props)
        if(autoInit === true)   this._breakpointHandler()
    }

        //  up

    _breakpointHandler(e){
        if(this.lastBreakpointName !== this.currentBreakpointName)  this.onBreakpointChange(e)
        this.lastBreakpointName = this.currentBreakpointName
        if(this.isMobile){
            this.onMobileBreakpoint(e)
        }else if(this.isDesktop){
            this.onDesktopBreakpoint(e)
        }
    }

    onBreakpointChange(e){   return e   }   //  fires first
    onMobileBreakpoint(e){  return e    }
    onDesktopBreakpoint(e){ return e    }
    

    get isMobile(){ return window.innerWidth < breakpointWidth  }
    get isDesktop(){ return window.innerWidth >= breakpointWidth  }

    get currentBreakpointName(){    //  to access properties keyed by breakpoint state
        if(this.isMobile)    return 'mobile'
        if(this.isDesktop)   return 'desktop'
        console.error('unexpected breakpoint case at:',window.innerWidth)  //  this should never happen
        return false
    }
}