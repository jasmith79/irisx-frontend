export default class CustomComposedEvent extends Event  {
    constructor(type,options={}){
        super(type,{
            bubbles:(options.bubbles) ? options.bubbles : false,
            cancelable:(options.cancelable) ? options.cancelable : false,
            composed:(options.composed) ? options.composed : true  //  secret sauce part 1
        })
            //  so assign doesn't get mad, and these don't get misreported
        delete options.bubbles
        delete options.cancelable
        delete options.composed
        
        Object.assign(this,options) //  secret sauce part 2
    }
}