import {html} from '@polymer/lit-element'
export default html`<style>
    /*<pagenav.css>*/
    #topbar {
      width: 100vw;
      height: 84px;
      position: fixed;
      z-index: 1024;
      top: 0px;
      left: 0px;
      font-weight: lighter;
      color: white;
      background-color: var(--brand-primary); }
      @media (max-width: 819px) {
        #topbar {
          display: flex;
          justify-content: space-between;
          align-items: stretch;
          height: 70px; } }
      #page-title {
        flex: 1 1 100%;
        width: 100%;
        line-height: 70px;
        font-weight: bold;
        letter-spacing: 2px;
        text-align: center;
        color: var(--brand-secondary);
        display: none; }
        @media (max-width: 819px) {
          #page-title {
            display: block; } }
      #logo-link {
        height: 100%;
        float: left; }
        @media (max-width: 819px) {
          #logo-link {
            flex: 0 0 auto;
            float: none; } }
        #logo-link a {
          display: block;
          padding: calc(var(--ggutter) / 2) calc(var(--ggutter) * 2); }
          @media (max-width: 819px) {
            #logo-link a {
              padding: 12px; } }
          #logo-link a img {
            -o-object-fit: fill;
               object-fit: fill; }
            @media (max-width: 819px) {
              #logo-link a img {
                width: 48px;
                height: 48px; } }
      #menu-container {
        width: 100%; }
        @media (max-width: 819px) {
          #menu-container {
            flex: 0 0 auto;
            width: auto;
            float: none; } }
        #menu-container > summary {
          position: absolute;
          z-index: 1050;
          top: 18px;
          right: 18px;
          display: none;
          width: 36px;
          height: 36px;
          background: url("/img/icon-hamburger-fill.svg") no-repeat center center;
          background-size: 36px; }
          @media (max-width: 819px) {
            #menu-container > summary {
              display: block;
              position: static;
              padding: 0px 18px;
              height: 100%; } }
          #menu-container > summary > span {
            display: none; }
        #menu-container[open] > summary {
          background-image: url("/img/icon-hamburger-fill - gold.svg"); }
        #menu-container > .details-content {
          z-index: 1050;
          height: inherit; }
          #menu-container > .details-content ul {
            height: 84px; }
            #menu-container > .details-content ul li {
              height: 100%; }
              #menu-container > .details-content ul li a {
                display: flex;
                flex-direction: column;
                align-items: center;
                height: 100%;
                padding: 0px var(--ggutter);
                text-decoration: inherit;
                color: inherit; }
                #menu-container > .details-content ul li a > div {
                  flex: 0 0 36px;
                  display: flex;
                  justify-content: center;
                  align-items: center;
                  width: 36px;
                  height: 36px;
                  margin-top: var(--ggutter);
                  border-radius: 50%;
                  background-color: var(--brand-grey-dim); }
                  #menu-container > .details-content ul li a > div img {
                    display: block; }
                #menu-container > .details-content ul li a span {
                  flex: 0 1 100%;
                  margin-top: 4px; }
              #menu-container > .details-content ul li.active a {
                background-color: var(--brand-primary-dark);
                color: var(--brand-secondary); }
                #menu-container > .details-content ul li.active a > div {
                  background-color: var(--brand-secondary); }
            #menu-container > .details-content ul.left-links {
              float: left; }
              #menu-container > .details-content ul.left-links li {
                float: left;
                margin-left: var(--ggutter); }
            #menu-container > .details-content ul.right-links {
              float: right; }
              #menu-container > .details-content ul.right-links li {
                float: right;
                margin-right: var(--ggutter); }
              #signout-link > div {
                background-color: var(--brand-primary-dark) !important;
                border-radius: 50% !important;
                padding: 0px !important; }
              #signout-link > span {
                color: var(--brand-secondary); }
    
    @media (max-width: 819px) {
      #menu-container > .details-content {
        position: absolute;
        z-index: 10;
        top: 70px;
        left: 0px;
        width: 100%;
        background-color: var(--brand-primary); }
        #menu-container > .details-content ul {
          height: auto; }
          #menu-container > .details-content ul.left-links, #menu-container > .details-content ul.right-links {
            float: none;
            clear: both; }
            #menu-container > .details-content ul.left-links li, #menu-container > .details-content ul.right-links li {
              display: block;
              width: 100%;
              float: none;
              clear: both;
              margin: 0px;
              text-align: unset; }
              #menu-container > .details-content ul.left-links li a, #menu-container > .details-content ul.right-links li a {
                display: flex;
                flex-direction: row;
                align-items: center;
                padding: 0px 0px var(--ggutter) var(--ggutter); }
                #menu-container > .details-content ul.left-links li a > div > img, #menu-container > .details-content ul.right-links li a > div > img {
                  display: inline-block; }
                #menu-container > .details-content ul.left-links li a span, #menu-container > .details-content ul.right-links li a span {
                  margin-top: 10px;
                  display: inline-block;
                  text-indent: 1em; } }
    
    /*</pagenav.css>*/
</style>`