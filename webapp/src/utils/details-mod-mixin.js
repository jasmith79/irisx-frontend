/*

    markup pattern:    
        <details class="details-mod"><summary></summary>
            <div class="details-content"></div>
        </details>

    variant elements:
        details.details-mod-simple              no interaction intended with details-content - close on click
        details>.details-content .close-btn     close on click this
        details.modal-overlay                   adds an overlay layer as ::after content, click to close
	
	<details class="modal-overlay details-mod details-mod-simple"><summary></summary>
            <div class="details-content">
                <button class="close-btn"><span></span></button>
            </div>
        </details>

    Options:{
		'selector':{	//	if relevant details object matches selector
				//	return false to cancel default behavior
			documentClickHandler:function(e,details,target){}  //  provides the event, relevant details, and relevant target
			summaryBlurHandler:function(){}
			summaryClickHandler:function(){}
		}
	}

*/

//  import polyfillEdge from '../../../js/poly.details.js'

export const DetailsModMixin = (BaseClass,Options={handlers:[]})=>class extends BaseClass {

    firstUpdated(...args){    super.firstUpdated(...args)   //  if LitElement
        //  polyfillEdge(this)
        this._attachListeners()
    }

        //  attatch listeners

    _attachListeners(){

        this._documentClick = this._documentClick.bind(this)

        window.document.addEventListener('click',this._documentClick)
        /*
        this.shadowRoot.querySelectorAll('details.details-mod>summary').forEach(summary=>{
            this._summaryBlur = this._summaryBlur.bind(this,e,summary)
            this._summaryClick = this._summaryClick.bind(this,e,summary)
            summary.addEventListener('blur',this._summaryBlur)
            summary.addEventListener('click',this._summaryClick)
        })
        */
    }

    disconnectedCallback(...args){ super.disconnectedCallback(...args)
        window.document.removeEventListener('click',this._documentClick)
        /*
        this.shadowRoot.querySelectorAll('details.details-mod>summary').forEach(summary=>{
            summary.removeEventListener('blur',this._summaryBlur)
            summary.removeEventListener('click',this._summaryClick)
        })
        */
    }

        //  allow for mixin handlers to be overridden

    _fireCustomHandlers(details,handlerName,e,...args){
        let returnVal = true
        Options.handlers.forEach(obj=>{
            let handler = obj[handlerName]
            if(details.matches(obj.selector) && handler){   //  if the selector fits
                    //  TODO: handle multiple matches?
                returnVal = handler(e,details,...args)
            }
        })
        return returnVal    //  returns true by default, so it doesn't cancel the normal listener
    }

    _documentClick(e){   //  click anywhere outside to close
              //  seems hacky, but it works?
        //  console.log(e.target,e.currentTarget,e.srcElement,document.elementFromPoint(e.clientX,e.clientY))
        //  if(e.srcElement && e.srcElement.tagName.toLowerCase() === 'summary')    return  //  get out of here, Edge
        let target = (this.shadowRoot.elementFromPoint) ? this.shadowRoot.elementFromPoint(e.clientX,e.clientY) : e.srcElement
        //  console.log('details mod doc click:',target)
        this.shadowRoot.querySelectorAll('details.details-mod[open]').forEach(details=>{
            //  console.log('open details:',details,'checked against click target:',e.target.tagName,target)
            let closeBtn = details.querySelector('.close-btn')
                //  if the handler function returns false, then don't handle the rest of the event
            if(this._fireCustomHandlers(details,'documentClickHandler',e,target) !== false){
            //  if(true){
                if(details.classList.contains('details-mod-simple')){   //  always close
                    //  console.log('simple')
                    details.open = false
                }else if(!details.contains(target)){    //  close if this click was outside the details
                    //  console.log('not details')
                    details.open = false
                }else if(details.classList.contains('modal-overlay') && target === details){    //  close if modal overlay click
                    //  console.log('modal & details')
                    details.open = false
                }else if(closeBtn && closeBtn.contains(target)){  //  click close button, if it's there
                    //  console.log('close button')
                    details.open = false
                }
            }else{
                //  console.log('click handling canceled by custom handler')
            }
        })
    }
    /*
    _summaryBlur(e,summary){ //  focus away to close
        let details = summary.parentNode
        if(this._fireCustomHandlers(details,'summaryBlurHandler',e,this) !== false){
            if(details.classList.contains('details-mod-simple') && details.open === true) details.open = false
        }
    }
    _summaryClick(e,summary){   //  click summary again to close
        let details = summary.parentNode
        if(this._fireCustomHandlers(details,'summaryClickHandler',e,this) !== false){
            if(details.open === true){
                e.preventDefault()
                details.open = false
                return false
            }
        }
    }
    */
}