import {html} from '@polymer/lit-element'
export default html`<style>
    /*<dms.css>*/
    #page-container {
      display: flex;
      align-items: stretch;
      height: 100%;
      background-color: var(--brand-grey-light); }
    
    /*</dms.css>*/
</style>`