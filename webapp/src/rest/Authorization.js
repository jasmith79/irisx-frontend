import { ConfigAPI } from '/config/Config.js'
export const Authorization = class {
    static async login(name,password){  return new Promise((resolve,reject)=>{
        this.logout()   //  so if it fails, you lose your previous JWT
        let url = new URL(ConfigAPI.AuthEndpoint,ConfigAPI.APIRoot)
        let request = new Request(url,{
            method:'POST',
            headers:new Headers({	'Content-Type':'application/json'	}),
            body:JSON.stringify({
                name:name,
                password:password
            })
        })
        fetch(request).then(response=>{
            response.json().then(data=>{
                if(response.ok === true && response.status === 200 && data.tokenType && data.accessToken) this.JWT = `${data.tokenType} ${data.accessToken}`
                resolve(Object.assign(data,{status:response.status,ok:response.ok}))
            },error=>reject(error))
        },error=>reject(error))
    })}
        //  link to data store
    static get JWT(){  return window.localStorage.getItem('IRISx_JWT') }
    static set JWT(val){  return window.localStorage.setItem('IRISx_JWT', val)    }
    static logout(){    window.localStorage.removeItem('IRISx_JWT')    }
}