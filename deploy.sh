#!/bin/bash
VERSION=$(jq '.version' < webapp/package.json | sed s/\"//g)
aws s3 cp webapp/build/${VERSION}.zip s3://crc-iris-dev-artifacts/${VERSION}/static/${VERSION}.zip