import { LitElement, html } from '@polymer/lit-element'

import ModuleTemplateStyles from './module-template.css.js'

window.customElements.get('module-tag') || window.customElements.define('module-tag',
class extends LitElement    {
    render(){   return html`
        ${ModuleTemplateStyles}
        <h1>${this.foo}</h1>
    `}
    static get properties(){    return {
        foo:{type:String,reflect:true}
    }}
    constructor(){  super()
        foo = 'bar'
    }
	firstUpdated(props){
		
	}
	updated(props){
		
	}
})