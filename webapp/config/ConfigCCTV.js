/* ⚠    KEEP THIS FILE! If you need to make changes, copy and rename this default version for future reference!   ⚠ */

window.__IRISXConfig.CCTV = {
	
    Grids:[1,4,9,16],    //  controls which grid layout options are listed for monitors
    
	MonitorsPollingRate:6000,    //  refresh monitor list every X miliseconds

        //  camera thumbnails will be constructed thus: [CamThumbRoot]+[Camera.name]+[CamThumbExt]
    CamThumbRoot:'http://content.trafficwise.org/cctv/',
    CamThumbExt:'.jpg',

    CameraPollingRate:60000,    //  refresh camera list every X miliseconds

    Map:{
            //  Leaflet map will center on this point, at the provided zoom level
            //  https://leafletjs.com/reference-1.4.0.html#map-setview
        DefaultLat:39.76632525654491,
        DefaultLon:-86.15615844726564,
        DefaultZoom:12,
        MinZoom:7,
        MaxZoom:18,
            //  map location search will prioritize results by proximity to this point:
        GeoSearch:{
            APIRoot:'https://photon.komoot.de/api/',
            ResultsLanguage:'en',
            SearchLocation:{lat:39.76632525654491,lon:-86.15615844726564}
        },
        Locations:[ //  TODO: need real values for these
                //  Leaflet map will pan + zoom to fit this area in the map view
                //  https://leafletjs.com/reference-1.4.0.html#map-flytobounds
            {       name:'Central Indiana',
                NorthLat:40,
                EastLon:-86,
                SouthLat:39.5,
                WestLon:-86.5
            },{     name:'Northwest Indiana',
                NorthLat:40,
                EastLon:-86,
                SouthLat:39.5,
                WestLon:-86.5
            },{     name:'Southern Indiana',
                NorthLat:40,
                EastLon:-86,
                SouthLat:39.5,
                WestLon:-86.5
            },{     name:'Southwest Indiana',
                NorthLat:40,
                EastLon:-86,
                SouthLat:39.5,
                WestLon:-86.5
            }
        ]
    },
    Table:{
        Pagination:[5,15,50,100]
    }
}