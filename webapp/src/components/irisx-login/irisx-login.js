﻿import { LitElement, html } from '@polymer/lit-element'
import { connect } from 'pwa-helpers/connect-mixin.js'
import { store } from '../../redux/redux-store'
import { navigate } from 'lit-redux-router'
import { DetailsModMixin } from '../../utils/details-mod-mixin.js'
import { ConfigLogin } from '/config/Config.js'

import { Authorization } from '../../rest/Authorization'

import CSSReset from '../css/lib/reset.css.js'
import BButtonBase from '../css/lib/bbuttonbase.css.js'
import SharedStyles from '../css/shared.css.js'
import LoginStyles from './login.css.js'
import CustomComposedEvent from '../../utils/CustomComposedEvent.js';

window.customElements.get('irisx-login') || window.customElements.define('irisx-login',
class extends connect(store)(DetailsModMixin(LitElement)) {
    render() {    return html`

        ${CSSReset}
        ${BButtonBase}
        ${SharedStyles}
        ${LoginStyles}

        <main id="login-container">
            <form id="login-form" role="main" @submit="${this._submitLogin}" @invalid="${{handleEvent:e=>this._invalidateLogin(e),capture:true}}">
                <fieldset>
                    <legend>
                        <h1><a href="${ConfigLogin.Logo.href}" id="logo-link"><img src="${ConfigLogin.Logo.icon}" alt="${ConfigLogin.Logo.alt}" /></a></h1>
                    </legend>
                    <ol class="formElements">
                    <div class="invalid-note invalid-login">Sorry, this Username and Password is incorrect.</div>
                        <li><input type="text" title="Username" name="login-username" required autofocus autocomplete="username" autocapitalize="none"  /><label for="login-username">Username</label>
                            <div class="invalid-note invalid-empty">You must enter a Username.</div>
                        </li>
                        <li><input type="password" title="Password" name="login-password" required autocomplete="current-password"  /><label for="login-password">Password</label>
                            <div class="invalid-note invalid-empty">You must enter a Password.</div>
                        </li>
                        <li><details class="form-tooltip-container details-mod details-mod-simple" id="forgot-password"><summary tabindex="0" @blur="${this._blurSummary}" @click="${this._clickSummary}">Forgot Password?</summary>
                            <div class="form-tooltip-content details-content">
                                <p>
                                    <strong>Forgot your password?</strong><br />If you are having difficulty with your password, please contact your system administrator to reset your password.
                                </p>
                            </div>
                            <button class="detailsOverlay bbuttonbase" @click="${this._clickSummary}"></button>
                        </details></li>
                        <li><button type="submit" name="login-submit" class="bbuttonbase cta">Sign In</button></li>
                    </ol>
                </fieldset>
            </form>
        </main>
    `}

    //  _EVENT LISTENERS

    _invalidateLogin(e){  //  check if it's empty
        e.preventDefault()
        e.target.classList.add('validated')
        let firstInvalidInput = e.target.form.querySelector('input:invalid')
        firstInvalidInput.focus()
        firstInvalidInput.select()
        return false
    }

    _clearField(input){
        input.classList.remove('invalid','validated')
        input.value = ''
        if(input.required === true){    //  seems silly, but this re-initializes the validation error message state
            input.required = false
            input.required = true
        }
    }

    _onLoginFail(loginForm,loginUsername,loginPassword){
        loginForm.classList.add('invalid-login')
        loginUsername.classList.add('invalid')
        loginPassword.classList.add('invalid')
        loginUsername.focus()
        loginUsername.select()
    }

    _onLoginSuccess(loginForm,loginUsername,loginPassword){
        loginForm.classList.remove('invalid-login')
        Array.from([loginUsername,loginPassword]).forEach(input=>this._clearField(input))
        loginForm.querySelectorAll(':focus').forEach(el=>el.blur())
        loginForm.reset()
        store.dispatch(navigate('/cctv'))
    }

    _submitLogin(e){  //  check if it's authentic
        let loginForm = e.target
        let loginUsername = loginForm['login-username']
        let loginPassword = loginForm['login-password']
        Authorization.login(loginUsername.value,loginPassword.value).then(result=>{
            if(result.ok === true && result.status === 200){
                if(Authorization.JWT)   this._onLoginSuccess(loginForm,loginUsername,loginPassword)
            }else if(result.status === 401){
                this._onLoginFail(loginForm,loginUsername,loginPassword)
            }else{
                console.error('unexpected error fetching login for auth:',result)
            }
        },error=>{
            console.error('unexpected error fetching login for auth:',error)
        })
        return e.preventDefault()
    }
})