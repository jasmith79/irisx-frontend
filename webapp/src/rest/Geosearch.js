import { ConfigCCTV } from '/config/Config.js'
export const GeoSearch = class {
    static async search(query){ return new Promise((resolve,reject)=>{

        let url = new URL(ConfigCCTV.Map.GeoSearch.APIRoot);    //  sometimes you gotta have semicolons

        ([
            ['q',query],
            ['lang',ConfigCCTV.Map.GeoSearch.ResultsLanguage],
            ['lat',ConfigCCTV.Map.GeoSearch.SearchLocation.lat],
            ['lon',ConfigCCTV.Map.GeoSearch.SearchLocation.lon],
            ['location_bias_scale',10],
        ]).forEach(param=>url.searchParams.set(param[0],param[1]))
        
        let request = new Request(url)

        fetch(request).then(response=>{
            response.json().then(data=>{
                resolve(data.features)
            },error=>reject(error))
        },error=>reject(error))
    })}
    static get mockResults(){   return {
        "type": "FeatureCollection",
        "features": [
            {
                "type": "Feature",
                "geometry": {
                    "coordinates": [
                    13.438596,
                    52.519854
                    ],
                    "type": "Point"
                },
                "properties": {
                    "city": "Berlin",
                    "country": "Germany",
                    "name": "Berlin"
                }
            },{
                "type": "Feature",
                "geometry": {
                    "coordinates": [
                    61.195088,
                    54.005826
                    ],
                    "type": "Point"
                },
                "properties": {
                    "country": "Russia",
                    "name": "Berlin",
                    "postcode": "457130"
                }
            }
        ]
    }}
}