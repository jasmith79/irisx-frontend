const Stickify = {
  _els:[],	//	collection of eligigible elements
  _changeState:el=>{  //  find the point where the bottom of the element should stick to the bottom of the screen
    let vHeight = document.documentElement.clientHeight
    let verticalSpace = vHeight - el.offsetTop
    let elHeight = el.scrollHeight
    let bottomOverrun = el.dataset.stickifyOffset || elHeight - verticalSpace
    if(bottomOverrun < 0){    //  it should probably be fixed in place
      el.classList.remove('stickify-tall')
      el.classList.add('stickify-short')
    }else{  //  it should probably be absolute in place, allowed to scroll
      el.classList.remove('stickify-short')
      el.classList.add('stickify-tall')
      let scrollDistance = document.documentElement.scrollTop
      if(!el.classList.contains('stickify-bottom') && scrollDistance > bottomOverrun){	//	element is not stuck but should be
        el.dataset.stickifyOffset = bottomOverrun
        el.classList.add('stickify-bottom')
      }else if(scrollDistance < bottomOverrun && el.classList.contains('stickify-bottom')){	//	element is stuck but shouldn't be
        el.classList.remove('stickify-bottom')
      }
    }
  },
  add:(...els)=>{	//	add one or more
    els.forEach(el=>{
      if(!Stickify._els.includes(el)){
        Stickify._els.push(el)	//	add new element(s) to the collection
      }
    })
    Stickify.hook() //	refresh the scroll event listener as needed
    Stickify._scrollHandler() //  first run
  },
  _scrollHandler:()=>Stickify._els.forEach(Stickify._changeState),
  hook:()=>window.addEventListener('scroll',Stickify._scrollHandler,{passive:true}),
  unhook:()=>window.removeEventListener('scroll',Stickify._scrollHandler,{passive:true})	//	cleanup
}
export default Stickify