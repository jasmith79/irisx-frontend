export const ADD_ITEM = 'ADD_ITEM'
export const REMOVE_ITEM = 'REMOVE_ITEM'
export const CLEAR_ITEMS = 'CLEAR_ITEMS'

export const addItem = (_source,_name) => (dispatch) => {
    dispatch({
        type:ADD_ITEM,
        source:_source,
        name:_name
    })
}

export const removeItem = (_source,_name) => (dispatch) => {
    dispatch({
        type:REMOVE_ITEM,
        source:_source,
        name:_name
    })
}

export const clearItems = (_source,_name) => (dispatch) => {
    dispatch({
        type:CLEAR_ITEMS,
        source:_source,
        name:_name
    })
}

function getTotalFromComponents(_components){
    let _total = 0
    if(_components.size === 0) return _total
    _components.forEach((_array)=>{
        _total += _array.length
    })
    return _total
}

export const LoaderAnim = (state = {
    components:new window.Map(),
    total:0
}, action) => {
    /*
        action.type:        ADD_ITEM, REMOVE_ITEM, CLEAR_ITEMS
        action.source:      web component reference (to actual node)
        action.name:        relevant event id (arbitrary string, may be multiple instances array keyed to source)
    */
    switch (action.type) {

        case ADD_ITEM:  return {    ...state,
            components:((source,name)=>{
                let queue = (state.components.has(source)) ? state.components.get(source) : new Array()
                queue.push(name)
                state.components.set(source,queue)
                return state.components
            })(action.source,action.name),
            total:getTotalFromComponents(state.components)
        }

        case REMOVE_ITEM:  return {    ...state,
            components:((source,name)=>{
                let _array = state.components.get(source)
                if(_array === undefined){
                    return state.components
                }else{
                    _array.splice(_array.indexOf(name),1) 
                    return state.components.set(source,_array)
                }
            })(action.source,action.name),
            total:getTotalFromComponents(state.components)
        }

        case CLEAR_ITEMS:  return {    ...state,
            components:((source,name)=>{
                    //  clear all
                if(source === undefined && name === undefined)  return new window.Map()
                    //  clear one component
                let _array = state.components.get(source)
                if(_array === undefined)    return state.components
                if(name === undefined)  return state.components.set(source,new Array())                
                    //  clear one component where name matches
                return state.components.set(source,_array.filter(_name=>_name!==name))
            })(action.source,action.name),
            total:getTotalFromComponents(state.components)
        }

        default:    return state
    }
}