import {html} from '@polymer/lit-element'
export default html`<style>
    /*<groupslist.css>*/
    :host {
      display: block; }
    
    @media (max-width: 819px) {
      :host {
        display: none;
        width: 100%;
        float: left;
        clear: both; }
      :host([mobilestate="MONITORS"]) {
        display: block; } }
    
    #group-list {
      position: static;
      width: 630px;
      overflow-y: auto;
      overflow-x: hidden;
      width: 100%; }
      @media (max-width: 819px) {
        #group-list {
          padding-bottom: 70px; } }
      #group-list > li {
        height: 86px; }
        #group-list > li a {
          display: flex;
          padding: calc(var(--ggutter) * 2) calc(var(--ggutter) * 4); }
          @media (max-width: 1134px) {
            #group-list > li a {
              padding: calc(var(--ggutter) * 2); } }
          #group-list > li a span {
            display: block;
            font-weight: bold;
            flex: 1 0 auto; }
          #group-list > li a ol {
            display: flex;
            flex: 0 1 auto; }
            #group-list > li a ol > li {
              margin-left: var(--ggutter); }
        #group-list > li:hover a {
          background: var(--brand-focus-light); }
        #group-list > li.active a {
          color: white;
          background: var(--brand-focus); }
          #group-list > li.active a aside {
            display: none; }
    
    /*</groupslist.css>*/
</style>`