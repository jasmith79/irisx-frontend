
    //  some simple shortcuts for LitHTML templating
    
export const PropIsVal=(prop,val)=>prop===val
const _StringIfPropIsVal = (result,prop,val)=>(PropIsVal(prop,val) ? result : '')
export {_StringIfPropIsVal as StringIfPropIsVal, _StringIfPropIsVal as ClassIfProp}

    //  deep property checking to avoid lengthy && chains
    //  A. Sharif (@sharifsbeat) https://twitter.com/sharifsbeat/status/843187365367767046

export const idx = (p=[],o={})=>p.reduce((xs, x) => (xs && xs[x]) ? xs[x] : null, o)

    //  round up to the nearest square

export const Round2Sq = x=>Math.pow(Math.ceil(Math.sqrt(x)),2)||1

    //  derive x/y coords from index of cell in a square grid, numbered ltr - and vice versa

export const i2xy=(i,s)=>[i%s,Math.floor(i/s)]
export const xy2i=(x,y,s)=>x+y*s