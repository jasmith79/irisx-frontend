import { LitElement, html } from '@polymer/lit-element'

import CSSReset from '../../css/lib/reset.css.js'
import BButtonBase from '../../css/lib/bbuttonbase.css.js'
import SharedStyles from '../../css/shared.css.js'

window.customElements.get('grid-icon') || window.customElements.define('grid-icon',
class extends LitElement    {
    render(){   return html`

        ${CSSReset}
        ${BButtonBase}
        ${SharedStyles}

        <style>
            ul  {
                display:grid;
                width:36px;
                height:24px;
                grid-gap:1px;
            }

            :host([count="1"]) ul { grid-template: 100% / 100%; }
            :host([count="4"]) ul { grid-template: repeat(2, 50%) / repeat(2, 50%); }
            :host([count="9"]) ul { grid-template: repeat(3, var(--t)) / repeat(3, var(--t));   }
            :host([count="16"]) ul {    grid-template: repeat(4, 25%) / repeat(4, 25%); }

            ul>li   {
                border-radius:3px;
                background:var(--brand-grey-dark);
            }

            :host([state="active"]) ul>li   {   background:var(--brand-focus);  }
            :host([state="disabled"]) ul>li   {   background:var(--brand-grey-light);  }
            ul>li>span  {   display:none;   }

        </style>
        <ul>${this.cells.map(cell=>html`
            <li><span>${cell}</span></li>
        `)}</ul>
    `}
    static get properties(){    return  {
        cells:{type:Array},
        count:{type:Number,reflect:true},
        state:{type:String,reflect:true}
    }}
    constructor(...args){  super(...args)
        this.cells = []
    }
    updated(props){ super.updated(props)
        if(props.has('count'))  this._buildCells()
    }
    _buildCells(){
        let _cells = []
        for(let i = 0; i < this.count; i++){
            _cells.push(i)
        }
        this.cells = _cells
    }
})