export const MONITORS = 'MONITORS'
export const CAMERAS = 'CAMERAS'   //  map or table
export const FILTERS = 'FILTERS'

export const SET_MOBILE_STATE = 'SET_MOBILE_STATE'
export const setMobileState = (_state) => (dispatch) => {
    dispatch({
        type:SET_MOBILE_STATE,
        state:_state
    })
}

export const MobileState = (state = {
    state:undefined
}, action) => {
    /*
        action.type:        SET_MOBILE_STATE
        action.state:       MONITORS, CAMERAS, FILTERS
    */
    switch (action.type) {
        case SET_MOBILE_STATE:  return {    ...state,
            state:action.state
        }
        default:    return state
    }
}