import { LitElement, html } from '@polymer/lit-element'

import CSSReset from '../css/lib/reset.css.js'
import BButtonBase from '../css/lib/bbuttonbase.css.js'
import SharedStyles from '../css/shared.css.js'
import CADStyles from './cad.css.js'

import '../common/irisx-page-nav.js'

window.customElements.get('irisx-cad') || window.customElements.define('irisx-cad',
class extends LitElement   {
    render() {return html`
        ${CSSReset}
        ${BButtonBase}
        ${SharedStyles}
        ${CADStyles}
        <irisx-page-nav page="cad"></irisx-page-nav>
        <main id="page-container">
            coming soon!
        </main>
    `}
})