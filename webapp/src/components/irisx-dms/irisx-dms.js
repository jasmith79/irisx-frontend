import { LitElement, html } from '@polymer/lit-element'

import CSSReset from '../css/lib/reset.css.js'
import BButtonBase from '../css/lib/bbuttonbase.css.js'
import SharedStyles from '../css/shared.css.js'
import DMSStyles from './dms.css.js'

import '../common/irisx-page-nav.js'

window.customElements.get('irisx-dms') || window.customElements.define('irisx-dms',
class extends LitElement {
    render(){   return html`

        ${CSSReset}
        ${BButtonBase}
        ${SharedStyles}
        ${DMSStyles}

        <irisx-page-nav page="dms"></irisx-page-nav>
        <main id="page-container">
            coming soon!
        </main>
    `}
})