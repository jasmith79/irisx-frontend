import { ConfigAPI } from '/config/Config.js'
import { Authorization } from '../rest/Authorization'
export const Monitors = class {
    static async get(name = ''){ return new Promise((resolve,reject)=>{

        let url = new URL(ConfigAPI.getMonitorsEndpoint(name),ConfigAPI.APIRoot)

        url.searchParams.set('controllerType','MONSTREAM')
        //  url.searchParams.set('cacheBuster',Date.now())
        
        let request = new Request(url,{
            method:'GET',
            headers:new Headers({
                'Authorization':Authorization.JWT,
                'Cache-Control':'no-store'
            })
        })

        fetch(request).then(response=>{
            response.json().then(data=>{
                resolve({response:response,data:data})
            },error=>reject(error))
        },error=>reject(error))
    })}
    static async set(name, attachedCameras){ return new Promise((resolve,reject)=>{

        let url = new URL(ConfigAPI.getSetMonitorEndpoint(name),ConfigAPI.APIRoot)
        
        let request = new Request(url,{
            method:'POST',
            headers:new Headers({
                'Authorization':Authorization.JWT,
                'Content-Type':'application/json'
            }),
            body:JSON.stringify({
                attachedCameras
            })
        })

       fetch(request).then(response=>{
        response.json().then(data=>{
            resolve({response:response,data:data})
        },error=>reject(error))
    },error=>reject(error))
    })}
    static get mockMonitor(){
        return  {name:'',notes:'',attachedCameras:[''],mock:true}
    }
    static get mockMonitors(){
        return  [Monitors.mockMonitor]
    }
}