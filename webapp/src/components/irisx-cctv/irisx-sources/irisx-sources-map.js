﻿import { LitElement, html } from '@polymer/lit-element'
import * as L from '../../common/leaflet-src.esm'
import { store } from '../../../redux/redux-store'
import { addItem, removeItem, clearItems } from '../../../redux/redux-LoaderAnim'
import { ConfigCCTV } from '/config/Config.js'
import CustomComposedEvent from '../../../utils/CustomComposedEvent'
import { BreakpointMixin } from '../../../utils/breakpoint-mixin'
import { CAMERAS, FILTERS } from '../../../redux/redux-MobileState';
import './irisx-collection-controls'

import CSSReset from '../../css/lib/reset.css.js'
import BButtonBase from '../../css/lib/bbuttonbase.css.js'
import SharedStyles from '../../css/shared.css.js'
import MapStyles from './map.css.js'

window.customElements.get('irisx-sources-map') || window.customElements.define('irisx-sources-map',
class extends BreakpointMixin(LitElement) {
    render() {return html`
    
        ${CSSReset}
        <link rel="stylesheet" href="/node_modules/leaflet/dist/leaflet.css" />
        ${BButtonBase}
        ${SharedStyles}
        ${MapStyles}

        <style>
            @media (max-width: 819px) {
                #sources-view-map {
                    height:calc(100vh - 70px - 60px - ${this.monitorOffsetY}px);
                }
            }
        </style>

        <div id="sources-view-map" class="${(this.mobilestate === CAMERAS || this.mobilestate === FILTERS) ? 'mobile-show' : ''}">
            <irisx-collection-controls
                view="map"
                group="${this.group}"
                mobilestate="${this.mobilestate}"
                @mapToLocation="${this._onMapToLocation}"
                @filtertypeschange="${this._onFilterTypesChange}"
                .cameraTypes="${this.cameraTypes}"
            ></irisx-collection-controls>
            <div id="map-container"></div>
            <template id="leaflet-popup">
                <figure class="popup-content"><img class="camera-still" src="img/slot-placeholder.jpg" width="356" height="200" /><figcaption><span class="cameraName"></span> (<span class="cameraID"></span>)<span class="camera-popup-status"></span></figcaption></figure>
            </template>
        </div>
    `}

    static get properties() {
        return {
                //  route segments
            view:{type:String,reflect:true},
            group:{type:String,reflect:true},
            mobilestate:{type:String,reflect:true},
                //  positioning
            monitorOffsetY:{type:Number,reflect:true},
                //  view stuff
            monitor:{type:Object},
            castingCam:{type:String},
            activeSlot:{type:Number},
            cameras:{type:Array},
            cameraTypes:{type:Array},
            markers:{type:window.Map},   //  map markers to IDs
            camIDs:{type:window.Map},   //  map IDs to markers
            camObjs:{type:window.Map},  //  map cam objects to IDs
            LMap:{type:L.map}   //  leaflet instance
        }
    }

    constructor(...args){   super(...args)

        store.dispatch(addItem(this,'init-render'))

        this._cameraClick = this._cameraClick.bind(this)
            //  these are maps in the 'collection' sense, not the the leaflet sense
        this.markers = new window.Map()
        this.camIDs = new window.Map()
        this.camObjs = new window.Map()
    }

    updated(props){   super.updated(props)
        if(props.has('view')){
            if(props.get('view') === 'map' && this.view !== 'map'){   //  switching away from map
                this.LMap.closePopup()
                store.dispatch(clearItems(this))
            }else if(props.get('view') !== 'map' && this.view === 'map'){  //  switching to map
                if(this.LMap !== undefined){
                    this.LMap.invalidateSize()
                }
            }
        }
        if(props.has('cameras')){
            this.LMap.closePopup()
            this._populateMap()
        }
        if(props.has('castingCam')){
            this.LMap.closePopup()
            this.markers.forEach(marker=>{
                let camID = this.camIDs.get(marker)
                if(camID === this.castingCam){
                    marker._icon.classList.add('irisx-active')
                }else{
                    marker._icon.classList.remove('irisx-active')
                }
            })
        }
        if(props.has('activeSlot') && this.activeSlot !== undefined){
            this.LMap.closePopup()
        }
        if(props.has('monitor')){
            //  console.log('map sees change:',this.monitor)
            this.markers.forEach(marker=>{
                let camID = this.camIDs.get(marker)
                //  console.log(marker,camID)
                if(this.monitor.attachedCameras.indexOf(camID) > -1){
                    marker._icon.classList.add('irisx-casted')
                }else{
                    marker._icon.classList.remove('irisx-casted')
                }
            })
        }
    }

    firstUpdated(...args){ super.firstUpdated(...args)

        let tileLayer = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            id: 'mapbox.streets',
            maxZoom: ConfigCCTV.Map.MaxZoom,
            minZoom: ConfigCCTV.Map.MinZoom,
            accessToken: 'pk.eyJ1IjoiY3JjLWRldiIsImEiOiJjaWY1b3NwOXcwMmtkc2trdDExOTA3Nmh2In0.eVQGwyWVr-zTFF5oyFU5wA',
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>'
        })

        tileLayer.on('load',()=>store.dispatch(removeItem(this,'init-render')))

        this.LMap = new L.map(this.shadowRoot.getElementById('map-container'),{
            center:[ConfigCCTV.Map.DefaultLat,ConfigCCTV.Map.DefaultLon],
            zoom:ConfigCCTV.Map.DefaultZoom
        }).addLayer(tileLayer)
    }

    _populateMap(){
        store.dispatch(addItem(this,'populate-map'))
        this._clearMarkers()
        if(this.cameras.length > 0) this.cameras.forEach(camera=>{  //  marker config
            let _marker = L.marker([camera.lat,camera.lon],{
                title:`${camera.displayName} (${camera.name})`,
                alt:`${camera.displayName} (${camera.name})`,
                icon:L.divIcon({
                    className:`
                        leaflet-custom-marker 
                        type-${camera.cameraType} 
                        status-${camera.status} 
                        ${((camera.name === this.castingCam) ? 'irisx-active' : '')} 
                        ${((this.monitor.attachedCameras.indexOf(camera.name) > -1) ? 'irisx-casted' : '')} 
                    `,
                    popupAnchor:[0,-14],
                    iconSize:L.point(36,36)
                }),
                interactive:true,
                riseOnHover:true
            })

            _marker.addTo(this.LMap)
            _marker.addEventListener('click',this._cameraClick)

                //  does this seem silly? It seems silly.
            this.markers.set(camera.name,_marker)
            this.camIDs.set(_marker,camera.name)
            this.camObjs.set(camera.name,camera)
        })
        
        this.LMap.invalidateSize()

        this.LMap._container.classList.add('map-ready') //  show the attribution link

        store.dispatch(removeItem(this,'populate-map'))
    }

    _breakpointHandler(...args){    super._breakpointHandler(...args)
        if(this.LMap)    this.LMap.invalidateSize()
    }

    _cameraClick(e){
        let camID = this.camIDs.get(e.target)
        if(this.activeSlot!==undefined){    //  now the event won't even fire without an active slot
            this.dispatchEvent(new CustomComposedEvent('camClick',{
                camera:camID
            }))
        }else{
            let camera = this.camObjs.get(camID)
            let marker = this.markers.get(camID)

            let popupContent = this.shadowRoot.getElementById('leaflet-popup').content.cloneNode(true).firstElementChild
            popupContent.id = 'camera-popup-' + camera.name
            popupContent.classList.add('type-'+camera.cameraType)
            popupContent.classList.add('status-'+camera.status)
            popupContent.querySelector('.cameraID').innerHTML = camera.name
            popupContent.querySelector('.cameraName').innerHTML = camera.displayName
            popupContent.querySelector('.camera-still').src = ConfigCCTV.CamThumbRoot+camera.name+ConfigCCTV.CamThumbExt

            let popup = L.popup({
                minWidth:360,
                maxWidth:580,
                maxHeight:505
            })
            
            popup.setContent(popupContent).setLatLng(marker._latlng).openOn(this.LMap)
        }
    }

    _clearMarkers(){
        this.markers.forEach(marker=>this.LMap.removeLayer(marker))
        this.markers.clear()
        this.camIDs.clear()
    }

    _onMapToLocation(e){
        this.LMap.fitBounds(
            L.latLngBounds(
                L.latLng(e.location.NorthLat,e.location.WestLon),
                L.latLng(e.location.SouthLat,e.location.EastLon)
            ),{
                maxZoom:14,
                padding:[20,20]
            }
        )
    }

    _onFilterTypesChange(){    this.dispatchEvent(new CustomComposedEvent('listCameras'))  }
})