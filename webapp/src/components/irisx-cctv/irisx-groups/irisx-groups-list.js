import { LitElement, html } from '@polymer/lit-element'
import { ClassIfProp } from '../../../utils/utils'
import { Round2Sq } from '../../../utils/utils'

import { Monitors } from '../../../rest/Monitors.js'

import CSSReset from '../../css/lib/reset.css.js'
import BButtonBase from '../../css/lib/bbuttonbase.css.js'
import SharedStyles from '../../css/shared.css.js'
import GroupsListStyles from './groupslist.css.js'

import './grid-icon.js'

window.customElements.get('irisx-groups-list') || window.customElements.define('irisx-groups-list', 
class extends LitElement    {
    render() {  return html`

        ${CSSReset}
        ${BButtonBase}
        ${SharedStyles}
        ${GroupsListStyles}

        <style>
            @media (min-width:820px)   {
                #group-list {
                    height:calc(100vh - calc(84px + ${this.monitorOffsetY}px));
                }
            }
            @media (min-width: 820px) and (max-width: 1129px) {
                #group-list {
                    margin-top:calc(${this.monitorOffsetY}px);
                }
            }
            @media (min-width:1130px)   {
                #group-list {
                    margin-top:calc(84px + ${this.monitorOffsetY}px);
                }
            }
        </style>

        <ol id="group-list">    ${this.monitors.map(monitor=>html`
            <li class="${ClassIfProp('active',this.group,monitor.name)}"><a href="/cctv/${monitor.name}/${this.view}"><span>${monitor.displayName || monitor.name || monitor.controllerName}</span><aside><grid-icon count="${Round2Sq(monitor.attachedCameras.length)}" state="disabled"></grid-icon></aside></a></li>
        `)} </ol>
    `}

    static get properties(){    return  {
            //  route
        view:{type:String,reflect:true},
        group:{type:String,reflect:true},
        mobilestate:{type:String,reflect:true},
        monitorOffsetX:{type:Number,reflect:true},
        monitorOffsetY:{type:Number,reflect:true},
            //  model
        monitors:{type:Array}
    }}
})