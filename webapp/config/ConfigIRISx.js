/* ⚠    KEEP THIS FILE! If you need to make changes, copy and rename this default version for future reference!   ⚠ */

window.__IRISXConfig.App = {

    AppTitle:'IRIS-x',  //  this will be used to populate the page title, e.g. 'AppTitle - PageTitle'

    Pages:{
        login:{ route:'/login',
            component:'/src/components/irisx-login/irisx-login.js',
            pageTitle:'Login'
        },
        cctv:{  route:'/cctv/:group/:view', //  pattern matching for the route
            defaultRouteSegments:{
                view:{  //  if you go to cctv/lobby/ - omitting the view value - the app will default to different paths depending on current layout type:
                    desktop:'map',
                    mobile:'table'
                }
            },
            component:'/src/components/irisx-cctv/irisx-cctv.js',   //  javascript file to load when page is first loaded
            pageTitle:'CCTV',   //  concatted with AppTitle and used as the <title> displayed in the browser
            
        },
        dms:{   route:'/dms',
            component:'/src/components/irisx-dms/irisx-dms.js',
            pageTitle:'DMS'
        },
        cad:{   route:'/cad',
            component:'/src/components/irisx-cad/irisx-cad.js',
            pageTitle:'CAD'
        }
    },

    SuperMenu:{
        Logo:{  //  the logo is the first item of the left of the menu
            icon:'/config/logo.png',
            alt:'Indiana Department of Transportation',
            href:'/cctv'    //  clicking the link goes here
        },
        Left:[  //  these links will run left-to-right next to the logo
            {   pageID:'cctv',  //  the slug used in the URL
                label:'CCTV',
                icon:'img/cctv-topbar-icon.svg',
                href:'/cctv'    //  the landing page URL, probably just the slug
            },{ pageID:'dms',
                label:'DMS',
                icon:'img/icon-sign-fill-solid.svg',
                href:'/dms'
            },{ pageID:'cad',
                label:'CAD',
                icon:'img/icon-dot-fill-solid.svg',
                href:'/cad'
            }
        ]
    }
}