import { html, LitElement } from '@polymer/lit-element'

import { ConfigCCTV } from '/config/Config.js'

import { DetailsModMixin } from '../../../utils/details-mod-mixin.js'
import { BreakpointMixin } from '../../../utils/breakpoint-mixin.js'
import { StringIfPropIsVal, idx } from '../../../utils/utils'
import CustomComposedEvent from '../../../utils/CustomComposedEvent'
import './irisx-collection-controls.js'

import '/node_modules/ag-grid-community/dist/ag-grid-community.min.noStyle.js'
import '/node_modules/ag-grid-polymer/index.js'

import CSSReset from '../../css/lib/reset.css.js'
import BButtonBase from '../../css/lib/bbuttonbase.css.js'
import SharedStyles from '../../css/shared.css.js'
import TableStyles from './table.css.js'
import { CAMERAS, FILTERS } from '../../../redux/redux-MobileState.js';

window.customElements.get('irisx-sources-table') || window.customElements.define('irisx-sources-table',
class extends BreakpointMixin(DetailsModMixin(LitElement)) {
  render() {  return html`

    ${CSSReset}
    ${BButtonBase}
    ${SharedStyles}
    <link rel="stylesheet" href="/node_modules/ag-grid-community/dist/styles/ag-grid.css">
    <link rel="stylesheet" href="/node_modules/ag-grid-community/dist/styles/ag-theme-material.css">
    ${TableStyles}

    <div id="sources-view-table" class="${(this.mobilestate === CAMERAS || this.mobilestate === FILTERS) ? 'mobile-show' : ''}">

      <irisx-collection-controls 
        view="table" 
        group="${this.group}" 
        mobilestate="${this.mobilestate}" 
        @cameraSearch="${this._onCameraSearch}"  
        @filtertypeschange="${this._onFilterTypesChange}"
        .cameraTypes="${this.cameraTypes}"
      ></irisx-collection-controls>

      <ag-grid-polymer
        class="ag-theme-material"
        @gridready="${this._gridReady}"
        @firstdatarendered="${this._firstDataRendered}"
        @rowDataChanged="${this._onRowDataChanged}"
        ._gridOptions="${this.gridOptions}"
      ></ag-grid-polymer>
      <footer id="pagination" class="${StringIfPropIsVal('show',this.showPagination,true)}">
          <details class="details-mod modal-overlay"><summary><span>${this.currentPage}</span> of <span>${this.totalCameras}</span> ${(this.searchstring !== '') ? `(results for "${this.searchstring}")` : ''}</summary>
              <div class="details-content">
                <h2>Show</h2>
                <ol id="pageSizeOptions">${ConfigCCTV.Table.Pagination.map(_pageSize=>html`
                  <li><label><input type="radio" name="pageSize" value="${_pageSize}" @change="${this._setPageSize}"><span>${_pageSize}</span></label></li>
                `)}</ol>                
              </div>
          </details>
          <nav>
              <button type="button" class="bbuttonbase" @click="${this._gridPageLast}" title="previous page" id="lastPage"><img src="img/icon-arrow-right-stroke.svg" alt="left arrow" width="36" height="36" /></button>
              <button type="button" class="bbuttonbase" @click="${this._gridPageNext}" title="next page" id="nextPage"><img src="img/icon-arrow-right-stroke.svg" alt="right arrow" width="36" height="36" /></button>
          </nav>
      </footer>
    </div>
  `}

  static get properties() { return {
      //  routing
    group:{type:String,reflect:true},
    view:{type:String,reflect:true},
    mobilestate:{type:String,reflect:true},

    cameraTypes:{type:Array},
    searchstring:{type:String,reflect:true},

      //  view
    cameras:{type:Array},
    monitor:{type:Object},
    castingCam:{type:String},

    columnDefs:{type:Array},
    gridOptions:{type:Object},
    gridApi:{type:Object},

    showPagination:{type:Boolean},
    currentPage:{type:String},
    pageSize:{type:Number},
    totalCameras:{type:Number,reflect:true}
  }}

    //  INIT

  constructor(){  super()

    this.searchstring = ''

      //  re-bind this for the benefit of ag grid native event listeners

    this._gridReady = this._gridReady.bind(this)
    this._firstDataRendered = this._firstDataRendered.bind(this)
    this._setPageSize = this._setPageSize.bind(this)
    this._updatePaginationText = this._updatePaginationText.bind(this)
    this._gridRowsChanged = this._gridRowsChanged.bind(this)
    this._rowClicked = this._rowClicked.bind(this)

      //  property init values

    this.columnDefs = [
      { headerName:'Type',
        field:'type',
        hide:true,  //  because this is only used for filtering
        suppressResize:true,
        suppressMovable:true,
        filter:'agTextColumnFilter'
      },{ headerName:'Status',
        autoHeight:true,
        minWidth:90,
        maxWidth:170,
        colId:'status',
        suppressFilter:true,
        suppressResize:true,
        suppressMovable:true,
          //  string used for sort / filter
        valueGetter: params=>params.data.cameraType+'-'+params.data.status,
          //  html used for display
        cellRenderer:params=>`
          <div class="list-icon type-${params.data.cameraType} status-${params.data.status}">
            <span><img src="${(params.data.cameraType === 'CCTV') ? 'img/icon-camera-fill-solid.svg' : 'img/icon-truck.svg' }" width="24" /></span>
          </div>
        `
      },{ headerName:'Name',
        field:'displayName',
        suppressFilter:true,
        suppressResize:true,
        suppressMovable:true
      },{ headerName:'ID',
        field:'name',
        minWidth:150,
        maxWidth:160,
        suppressFilter:true,
        suppressResize:true,
        suppressMovable:true
      }
    ]
    this.pageSize = 5

    this.gridOptions = {
      columnDefs:this.columnDefs,
      rowData:[],
      getRowNodeId:data=>data.name,
      domLayout:'autoHeight',
      suppressRowClickSelection:true,
      pagination:true,
      paginationPageSize:this.pageSize,
      suppressPaginationPanel:true,
      onPaginationChanged:this._updatePaginationText,
      enableSorting:true,
      onRowClicked:this._rowClicked,
      suppressCellSelection:true,
      onModelUpdated:this._gridRowsChanged,
      rowClassRules:{
        'irisx-active':params=>params.data.name===this.castingCam,
        'irisx-casted':params=>this.monitor.attachedCameras.includes(params.data.name)
      }
    }
  }

  updated(props){   super.updated(props)
    if(this.view === 'table'){  //  currently applicable
      if(
        props.has('view') ||
        (props.has('mobilestate') && this.mobilestate === CAMERAS)
      ){
        let rowsToDisplay = idx(['rowModel','rowsToDisplay'],this.gridApi) || []
        if(rowsToDisplay.length === 0){
          this._populateTable()
        }
        this._resizeGrid()
      }
    }
    if(props.has('cameras'))  this._populateTable()
    if(props.has('monitor') || props.has('castingCam')){
      //  console.log('updating rows...',this.monitor,this.castingCam)
      if(this.gridApi)  this.gridApi.redrawRows()
    }
    if(props.has('group'))  this._resizeGrid()  //  fixes page switching layout issues
  }

  _breakpointHandler(...args){  super._breakpointHandler(...args)
    if(this.isMobile){
      if(this.mobilestate === CAMERAS)   this._resizeGrid()
    }else if(this.isDesktop){
      this._resizeGrid()
    }
  }

    //  CALLBACKS + EVENTS

  _onRowDataChanged(e){
    console.log(e)
  }

  _populateTable(){
    this.totalCameras = this.cameras.length
    if(this.gridApi){
      this.gridApi.setRowData(this.cameras)
    }else{
      //  console.info('tried to populate table but gridApi not yet available...')
    }
  }

    //  AG GRID HOOKS

  _gridReady(e){
    //  console.log('grid ready!')
    if(e.agGridDetails){
      this.gridApi = e.agGridDetails.api
      this.shadowRoot.querySelector('input[value="'+this.pageSize+'"]').checked = true
      if(this.cameras)  this._populateTable()
    }
  }

  _firstDataRendered(){
    //  console.log('first data rendered!')
    this._resizeGrid()
  }

    //  OTHER

  _resizeGrid(){
    if(this.gridApi && this.view === 'table'){
      this.gridApi.sizeColumnsToFit()
      this.gridApi.resetRowHeights()
    }
  }
      
  _updatePaginationText(){
    if(this.gridApi){
      this.totalCameras = this.gridApi.paginationGetRowCount()
      let currentPageIndex = this.gridApi.paginationGetCurrentPage()
      let firstIndex = currentPageIndex * this.pageSize
      let lastIndex = (firstIndex + this.pageSize > this.totalCameras) ? this.totalCameras : firstIndex + this.pageSize
      this.currentPage = `${firstIndex + 1} - ${lastIndex}`

      this.shadowRoot.getElementById('lastPage').disabled = (currentPageIndex <= 0) ? true : false
      this.shadowRoot.getElementById('nextPage').disabled = (currentPageIndex === this.gridApi.paginationGetTotalPages() - 1) ? true : false

    }
  }

  _setPageSize(e){
    this.pageSize = parseInt(e.target.value)
    this.gridApi.paginationSetPageSize(this.pageSize.toString())
    e.target.closest('details').open = false
  }

  _gridPageLast(){  this.gridApi.paginationGoToPreviousPage() }

  _gridPageNext(){  this.gridApi.paginationGoToNextPage() }

  _rowClicked(e){
    this.dispatchEvent(new CustomComposedEvent('camClick',{
      camera:e.data.name
    }))
  }

  _onCameraSearch(e){
    this.searchstring = e.query
    this.gridApi.setQuickFilter(e.query)
  }

  _gridRowsChanged(){
    if(this.gridApi)  this.showPagination = (this.gridApi.getDisplayedRowCount() > 0) ? true : false
  }

  _onFilterTypesChange(){  this.dispatchEvent(new CustomComposedEvent('listCameras')) }
})